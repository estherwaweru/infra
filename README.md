# **Table of Contents**
- [Frontend](https://gitlab.com/estherwaweru/infra/-/edit/main/README.md#frontend)
- [Backend](https://gitlab.com/estherwaweru/infra/-/edit/main/README.md#backend)
- [IAC](https://gitlab.com/estherwaweru/infra/-/edit/main/README.md#iac)
- [CICD](https://gitlab.com/estherwaweru/infra/-/edit/main/README.md#cicd)
- [URLS](https://gitlab.com/estherwaweru/infra/-/edit/main/README.md#application-urls)

# Frontend
- Navigate the frontend folder by `cd fronend`
- Add ._env_ file to the root folder of fronend
- Copy the contents of _example.env_ and add the right values to the keys
- Ensure you have **npm** installed in your machine
- Install packages by `npm install`
- Run application locally by `npm start`
- If you want to use docker to run the application locally run `docker build -t fronend .` then `docker run -p 127.0.0.1:3000:3000 fronend`

# Backend
- Navigate to backend folder by `cd backend`
- Add ._env_ file to the root folder of backend.
- Copy the contents of _example.env_ and add the right values to the keys
- Ensure you have **python** installed in your machine.
- Install packages by `pip install -r requirements.txt`
- Run the application locally by `flask run`. 
- Use any database engine of your choice i.e sql,postgresql,mysql etc 
- Initialize the database by `falsk db init` the `flask db migrate` then `flask db upgrade`
- If you want to use docker to run the application locally run `docker compose build` then `docker compose up`

# IAC
- Ensure that terraform is installed in your machine. If it's not follow this [guide](https://developer.hashicorp.com/terraform/install)
- Ensure you have a GCP account. If you don't have an account follow this [guide](https://cloud.google.com) then create an account
- Create a project in GCP console then copy the project id which you can plug it in your environment. `export google_project_id = google_project_id`  [for more](https://developer.hashicorp.com/terraform/cli/config/environment-variables)
- Install **google cloud cli** to your machine . Follow this [guide] (https://cloud.google.com/sdk/docs/install) 
- Run `gcloud auth application-default login` to authenticate terraform so that it can create your infrastructure
- `cd iac`
- `terraform init`
- `terraform plan`
- **Note**: Some services have to be enabled first in GCP console such as **Cloud Run Admin Api**  and **Artifact registry Api**, **Cloud Sql Admin Api** for a successful infrastructure creation
- Then run `terraform apply` to provision the infrastrure.


# CICD
- This project utilizes Gitlab CI. To reproduce this follow the following steps:
- Create a Gitlab [account] (https://gitlab.com/users/sign_in) if you don't have one
- Head over to variables [section](https://gitlab.com/estherwaweru/infra/-/settings/ci_cd) and add the following variables: `DB_CONNECTION,DB_NAME,DB_PASSWORD,ENV,FLASK_APP,GCLOUD_KEY,GCLOUD_PROJECT,GCLOUD_REGION,GCLOUD_REPOSITORY,REACT_APP_BACKEND_URL,SQLALCHEMY_DATABASE_URI`
The values should be obtained as follows

| KEY | WHERE TO OBTAIN VALUE |
| ------ | ------ |
|   DB_CONNECTION     |  copy this from your GCP project Cloud sql Connection name or from teraform output values      |
|   DB_NAME     |    copy this from GCP project Cloud Sql database    |
| DB_USER | copy this from GCP project Cloud Sql user|
|DB_PASSWORD|Generate this for the user you want to use and copy it to this variable|
|GCLOUD_KEY|Create a service account user and grant Editor roles for Cloud Sql, Artifact Registry and Cloud Run then generate the key as json and copy the raw json contents|
|GCLOUD_REGION| us-central1|
|GCLOUD_REPOSITORY|grab this from the artifact registry repository name|
|ENV|PROD|
|FLASK_APP|main.py|
|REACT_APP_BACKEND_URL|Grab this from backend cointainer url|

- The pipeline involves the following stages: **linting, build and deploy**
- Make any changes to anything in backend folder and commit the changes to trigger CICD for backend
- Make any changes to anything in frontend folder and commit the changes to trigger pipeline for frontend
- Make any changes to .gitlab-ci file and commit the change to trigger pipeline for backend and frontend

# Application URLS
- [Frontend](https://frontend-application-o4gv3hgb3a-uc.a.run.app)
- [Backend](https://backend-application-o4gv3hgb3a-uc.a.run.app)



