import os

from flask import Flask, jsonify, Response
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from google.cloud.sql.connector import Connector


app = Flask(__name__)
CORS(app)

db = SQLAlchemy()
migrate = Migrate(app, db)

app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get("SQLALCHEMY_DATABASE_URI")
if os.environ.get("ENV", "DEV") == "PROD":
    connector = Connector()

    # Python Connector database connection function
    def getconn():
        conn = connector.connect(
            os.environ.get("DB_CONNECTION"),  # Cloud SQL Instance Connection Name
            "pg8000",
            user=os.environ.get("DB_USER"),
            password=os.environ.get("DB_PASSWORD"),
            db=os.environ.get("DB_NAME"),
            ip_type="public",  # "private" for private IP
        )
        return conn

    app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {"creator": getconn}
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

# initialize the app with Flask-SQLAlchemy
db.init_app(app)


# message Model
class Message(db.Model):
    __tablename__ = "message"

    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String)
    created_at = db.Column(db.DateTime)

    def __repr__(self):
        return f"Message: {self.text}"


@app.route("/")
def is_alive():
    return jsonify("live")


@app.route("/api/msg/<string:msg>", methods=["POST"])
def msg_post_api(msg):
    print(f"msg_post_api with message: {msg}")
    # store msg in DB and return identifier
    new_message = Message(text=msg)
    db.session.add(new_message)
    db.session.commit()
    return jsonify({"msg_id": new_message.id})


@app.route("/api/msg/<int:msg_id>", methods=["GET"])
def msg_get_api(msg_id):
    print(f"msg_get_api > msg_id = {msg_id}")
    # get msg from DB and return it
    msg = Message.query.filter_by(id=msg_id).first()
    if not msg:
        return Response("Message not found", status=404)
    return jsonify({"msg": msg.text})


if __name__ == "__main__":
    if os.environ.get("ENV", "DEV"):
        app.run(debug=True, host="127.0.0.1")
    app.run(debug=False, host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))
