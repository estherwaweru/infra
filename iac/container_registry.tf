resource "google_artifact_registry_repository" "my-repo" {
  location      = "us-central1"
  repository_id = "infra-project-repository"
  description   = "example docker repository"
  format        = "DOCKER"

}