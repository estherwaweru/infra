
resource "google_cloud_run_v2_service" "default" {
  name     = "frontend-application"
  location = "us-central1"

  template {
    scaling {
      max_instance_count = 2
    }

    containers {
      name  = "frontend-application"
      image = "us-docker.pkg.dev/cloudrun/container/hello"
      ports {
        container_port = 3000
      }
    }
  }


  traffic {
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
    percent = 100

  }
}

resource "google_cloud_run_v2_service" "backend" {
  name     = "backend-application"
  location = "us-central1"

  template {
    scaling {
      max_instance_count = 2
    }

    containers {
      name  = "backend-application"
      image = "us-docker.pkg.dev/cloudrun/container/hello"
      ports {
        container_port = 5000
      }
    }

  }


  traffic {
    type    = "TRAFFIC_TARGET_ALLOCATION_TYPE_LATEST"
    percent = 100

  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_v2_service_iam_policy" "noauth" {
  location    = google_cloud_run_v2_service.default.location
  project     = google_cloud_run_v2_service.default.project
  name        = google_cloud_run_v2_service.default.name
  policy_data = data.google_iam_policy.noauth.policy_data
}

resource "google_cloud_run_v2_service_iam_policy" "backend_noauth" {
  location    = google_cloud_run_v2_service.backend.location
  project     = google_cloud_run_v2_service.backend.project
  name        = google_cloud_run_v2_service.backend.name
  policy_data = data.google_iam_policy.noauth.policy_data
}

